function bundle {
   local ext=$(basename ${SHELL})
   local folder="${HOME}/.config/bundle/source.d /etc/bundle/source.d"

   case "$1" in
      av|avail)
         for d in ${folder}
         do
            if [ -d "$d" ]
            then
               echo "   --- folder $d"
               ( cd "$d" ; find . -type f -a -name "*.${ext}" | sed -e "s/.\///; s/\.${ext}\$//" )
            fi
         done
         ;;

      lo|load)
         pack=$(find ${folder} -type f -a -name "*.${ext}" -print0 2> /dev/null | grep -FzZ "/$2.${ext}" | head -n 1 )
         if [ -f "${pack}" ]
         then
            export _bundle_name="$2"
            let _bundle_level++
            export _bundle_level
            export _bundle_status="${_bundle_status}:$2"
            [ -n "$PS1" ] && history -w
            $VERBOSE && echo "Begin bundle: ${_bundle_name}"
            ${SHELL} --rcfile ${pack}
            $VERBOSE && echo "End bundle: ${_bundle_name}"
            [ -n "$PS1" ] && history -r
            unset _bundle_name
            let _bundle_level--
            [ ${_bundle_level} -eq 0 ] && unset _bundle_level
            export _bundle_status=$(echo "${_bundle_status}" | sed 's/:[^:]*$//')
            [ "${_bundle_status}" == "" ] && unset _bundle_status
         else
            echo "unable to load bundle file: $2"
            echo "Usage: bundle load bundle-file"
         fi
         ;;

      un|unload)
         if [ ${_bundle_level} > 0 ]
         then
            [ -n "$PS1" ] && history -w
            exit
         else
            echo "Error: not a bundle environment!"
         fi
         ;;

      sh|show)
         pack=$(find ${folder} -type f -a -name "*.${ext}" -print0 2> /dev/null | grep -FzZ "/$2.${ext}" | head -n 1 )
         if [ -f "${pack}" ]
         then
            pod2text ${pack}
         else
            echo "unable to find bundle file: $2"
            echo "Usage: bundle show bundle-file"
         fi
         ;;

      li|list)
         if [ ${_bundle_level} > 0 ]
         then
            echo "${_bundle_status}" | sed 's/^://'
         else
            echo "Error: not a bundle environment!"
         fi
         ;;

      st|status)
         if [ ${_bundle_level} > 0 ]
         then
            echo "bundle environment active"
         else
            echo "not a bundle environment"
         fi
         ;;

      so|source)
         pack=$(find ${folder} -type f -a -name "*.${ext}" -print0 2> /dev/null | grep -FzZ "/$2.${ext}" | head -n 1 )
         if [ -f "${pack}" ]
         then
            . ${pack}
         else
            echo "Usage: bundle source bundle-file"
         fi
         ;;

      *)
         echo "Usage: bundle list|avail|load|unload|show|status|source|help"
         ;;
   esac
   }

typeset -fx bundle

return

################################################################
# Documentation in POD format (like Perl)
################################################################

=head1 NAME

bundle - load specific environment in current shell

=head1 SYNOPSIS

 bundle avail
 bundle show bundle-file

 bundle load bundle-file
 bundle unload
 bundle list

 bundle source bundle-file

 bundle status

 bundle help

=head1 DESCRIPTION

C<bundle> can load a new environment in current shell
(source) or load it in a sub-shell.
In this second case, environment could be unload!

This shell function looks furiously at the command C<module>.
It's wanted but is much simpler (no C<tcl>...).
The arguments are quite the same.
There is no magic done on the current environment variables
(remember, C<bundle> just source shell file).

C<bundle> launch a new shell (sub-shell) at load and exit it at unload.
It's all!

No...
In interactive mode, it's save the current history so this one can be use in the sub-shell.
The same thing is done at the end of the sub-shell,
history is then reload in the master shell.
For the user point of vu, it's like there is only one shell!

But be carrefull with variable,
only export variable could be use in sub-shell.


=head1 COMMAND

=over 12

=item B<avail> 

List all available bundle.
Bundle are search in two specific path:
F<${HOME}/.config/bundle/source.d> and F</etc/bundle/source.d>

Bundle are just shell script with the shell name as extension
(C<.bash> for C<bash> script).
Bundle are first search in user folder.
This allows the user to overloaded a system bundle.

=item B<show> F<bundle-file>

Show a small description of the bundle file if available...
Format is done width the command C<pod2text>.
Documentation can be written at the end of the script after a last command C<return> for C<bash>.

=item B<load> F<bundle-file>

Start a new shell and source bundle file inside.

=item B<unload>

End of bundle specific shell.

=item B<list>

List loaded bundle.

=item B<source> F<bundle-file>

Source bundle in current shell.
Environment could not be unload...

=item B<status>

Indicates whether we are in a bundle or not.

=item B<help>

Usage line.

=back


=head1 EXAMPLE

Which is better : load or source ?

=head2 Sub-Shell

Load and Unload command must be enclose.
In an interactive shell,
it's very important to unload every bundle loaded!

 bundle load intel/2011.7
   ifort -v
 bundle unload

Load command in better in interactive shell.

If use in a script file,
you just have to remenber that load start a new shell,
and unload just exit it (but not work in script file)

 bundle load intel/2011.7 <<'END_BUNDLE'
   ifort -v
 END_BUNDLE

It's better to use quote around END_BUNDLE,
current shell will not evaluate next command before passing them to sub-shell !

Maybe it's then simpler to use source command and integrated sub-shell

 ( bundle source intel/2011.7
   ifort -v
 )

=head2 Source

Source command just source shell script in current environment.
No unload is possible.

 bundle source intel/2011.7

Source command is better in batch script
because you have don't have to unload in current case.


=head1 SEE ALSO

sysprofile, module


=head1 AUTHORS

Written by Gabriel Moreau, Grenoble - France


=head1 LICENSE AND COPYRIGHT

GPL version 2 or later

Copyright (C) 2011-2012 Gabriel Moreau / LEGI - CNRS UMR 5519 - France
