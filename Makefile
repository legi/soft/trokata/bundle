
# General configuration

PODFLAG := -c 'User Profile Documentation' -r 'LEGI Trokata'

TARGET_MAN  := bundle.1
TARGET_HTML := bundle.html

PREFIX     :=/etc/sysprofile.d
PREFIX_MAN :=/usr/share/man


# User specific configuration

sinclude config.mk


# Target and rules

.PHONY: all clean install update sync upload

all: $(TARGET_MAN) $(TARGET_HTML)

clean:
	rm -f *.1 *.html

install: update

update:
	@install -d -m 0755 -o root -g root $(PREFIX_MAN)/man1
	install -m 0644 -o root -g root bundle.bash $(PREFIX)/
	install -m 0644 -o root -g root bundle.1 $(PREFIX_MAN)/man1

sync:
	svn update

upload:
	cadaver --rcfile=cadaverrc

%.1: %.bash
	pod2man -n $* $(PODFLAG) $< > $@

%.html: %.bash
	pod2html $< > $@

# User specific rules

sinclude rules.mk
