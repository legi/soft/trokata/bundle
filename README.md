# Bundle - Gestion de l'environnement utilisateur

[bundle](https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/bundle) est une fonction `bash`
qui permet de charger ou de décharger un environnement dans son shell courant.
Pour le moment, seul `bash` est réellement testé !

`bundle` dans l'esprit est très proche de [module](http://modules.sourceforge.net/).
Cette dernière modifie en temps réel les variables d'environnement du shell courant.
Elle se configure via le langage `tcl`.

`bundle` a une architecture beaucoup plus simple.
Il consiste essentiellement à lancer un nouveau shell via la commande `load`
et à fermer ce shell via `unload`.
Lors du chargement, le fichier en paramètre est sourcé et modifie donc l'environnement courant...
Il est ainsi possible de définir aussi des fonctions en plus des variables d'environnement.
La configuration est simplement un fichier comme le `bashrc` par exemple.

Contrairement à `module`,
`bundle` fonctionne avec des sous shell (sauf commande `source`).
Les variables non exportés ne sont pas utilisable dans le sous shell.
Autre différence, comme en programmation moderne,
les `bundle` doivent être chargé et déchargé dans l'ordre
alors que `module` autorise un ordre quelconque.

Moyennant un petit hack, il faut noter que l'history du shell est conservé !
Un utilisateur n'y prêtant pas attention ne verra pas qu'il passe dans un sous shell...
Par contre, si une variable est modifiée, en sortie de sous shell,
elle retrouve sa valeur d'avant (si elle existe) !
Dans ce cas là, il est préférable de faire un `source` et de rester ainsi dans le même shell.

## Repository

L'ensemble du code est sous **licence libre**.
Les scripts en `bash` sont sous GPL version 3 ou plus récente (http://www.gnu.org/licenses/gpl.html).

Tous les sources sont disponibles sur la forge du LEGI :
http://servforge.legi.grenoble-inp.fr/svn/soft-trokata/trunk/bundle

Les sources sont gérés via Git.
Il est très facile de rester synchronisé par rapport à ces sources.

 * la récupération initiale
   ```bash
   git clone https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/oarutils soft-oarutils
   ```

 * les mises à jour par la suite
   ```bash
   git pull
   ```

Il est possible d'avoir un accès en écriture à la forge
sur demande motivée à [Gabriel Moreau](mailto:Gabriel.Moreau__AT__legi.grenoble-inp.fr).
Pour des questions de temps d'administration et de sécurité,
la forge n'est pas accessible en écriture sans autorisation.

## Installation

Une fois les sources récupérée via Git,
il faut le compiler avant l'installation.
Ici, la compilation va essentiellement fabriquer la documentation !

Le projet a quelques dépendances écrites ici sous forme de paquets debian.
La transposition est facile à faire :

 * obligatoire :
   `apt-get install perl`

 * facultative :
   `apt-get install sysprofile lsb-core`

Un `Makefile` est à la racine du projet,
il comporte les cibles suivantes :

 * `all` : Compilation de la documentation

 * `install` : Installation à faire la première fois.

 * `update` : Mise à jour de l'installation.
   Ne mettra pas à jour certains fichiers si besoin (configuration...).

 * `sync` : Mise à jour vis à vis du dépôt Git.
   En pratique, équivalent à `git pull`.

Il ne faut pas modifier le `Makefile` pour l'adapter à son environnement.
Cependant, celui-ci n'est pas forcément idéal pour votre configuration.
Pour ce faire, le `Makefile` charge s'ils existent les deux sous fichiers `config.mk` et `rules.mk`.
Le premier permet de surcharger des variables et le second des règles.

Par défaut, `bundle` fonctionne avec le paquetage debian `sysprofile`
et va s'installer dans `/etc/sysprofile.d`.
Il peut être judicieux de l'installer ailleurs, par exemple dans `/etc/profile.d`
(paquetage debian `lsb-core`).
Cela se réalise très simplement en définissant dans le fichier `config.mk`
la variable `PREFIX`.

```bash
PREFIX:=/etc/profile.d
```
